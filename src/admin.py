from Crypto.PublicKey import RSA
from datetime import timedelta, datetime
from config import OLD_AGE_DEFAULT
import sys
import argparse
import base64
import uuid
import db
import os

os.chdir("..")

def with_default(maybe_none, default):

    return default if maybe_none is None else default


def add_authority(subparsers):
    """
    Add a new authority to the server and specify their
    abilities
    """
    parser = subparsers.add_parser("add-authority", help=__doc__)

    parser.add_argument("public-key",
                        type=str)

    parser.add_argument("--name",
                        type=str,
                        required=True)

    parser.add_argument("--is-auditor",
                        action=argparse.BooleanOptionalAction,
                        default=False)

    parser.add_argument("--is-decision-authority",
                        action=argparse.BooleanOptionalAction,
                        default=False)

    def func(args):

        is_code_auditor = args.is_auditor

        is_decision_authority = args.is_decision_authority

        public_key = getattr(args, "public-key")

        if public_key == "-":

            public_key = sys.stdin.read()

        else:

            public_key = open(public_key, "rb").read()

        public_key = RSA.import_key(public_key)

        authority = db.Authority.create(

            is_code_auditor = is_code_auditor,

            is_decision_authority = is_decision_authority,

            public_key = public_key.export_key(format="PEM"),

            name = args.name

            )

        print("Generated authority", authority.id)

    parser.set_defaults(func=func)


def rm_authority(subparsers):
    """
    Remove an authority by way of it's id
    """

    parser = subparsers.add_parser("rm-authority", help=__doc__)

    parser.add_argument("id",
                        type=uuid.UUID)
    def func(args):

        success = db.Authority.delete().where(db.Authority.id == args.id).execute() == 1

        print("Success" if success else "Failure")

    parser.set_defaults(func=func)


def clean_db(subparsers):
    """
    Remove old decisions, accounts and voting methods
    """

    parser = subparsers.add_parser("clean-db", help=__doc__)

    parser.add_argument("--old-age",
                        help="Age in days for things to be considered old, overriden by other age options",
                        default=OLD_AGE_DEFAULT,
                        type=lambda d: timedelta(days=d))

    parser.add_argument("--decision-age",
                        help="Age in days for decisions to be considered old",
                        type=lambda d: timedelta(days=d))

    parser.add_argument("--accounts-age",
                        help="Age in days for accounts to be considered old",
                        type=lambda d: timedelta(days=d))

    def func(args):

        old_age = args.old_age

        decision_age = datetime.now() + with_default(args.decision_age, old_age)

        accounts_age = datetime.now() + with_default(args.accounts_age, old_age)

        old_age = datetime.now() + old_age

        decisions_deleted = db.Decision.delete().where(db.Decision.schedule.end_voting_phase < decision_age).execute()

        accounts_deleted = db.Account.delete().where(db.Account.last_activity < accounts_age).where(db.Account.voting_methods.count() == 0).execute()

        print(f"Decisions deleted: {decision_deleted}\nAccounts deleted: {accounts_deleted}")

    parser.set_defaults(func=func)

def gen_key(subparsers):
    """
    generate a public and private key and store them in keys/ folder.
    """

    parser = subparsers.add_parser("gen-key", help=__doc__)

    parser.add_argument("name",
                        help="Name of the key pair. Used for the filename",
                        type=str)

    parser.add_argument("--bits",
                        help="Bit length of the key generated",
                        default=2048,
                        type=int)

    parser.add_argument("--algorithm", "-a",
                        help="Specify the key algorithm. rsa for JWT Keys. ecc for authorities.",
                        choices=["rsa", "ecc"],
                        default="ecc",
                        type=str)


    def func(args):

        print("Generating key...")

        private_key = RSA.generate(max(args.bits, 2048)) # 2048 is the minimum secure size

        private_key_path = f"keys/{args.name}"

        public_key_path = private_key_path + ".pub"

        with open("/" + private_key_path, "wb") as fh:

            fh.write(private_key.export_key(format="PEM"))


        with open("/" + public_key_path, "wb") as fh:

            fh.write(private_key.public_key().export_key(format="PEM"))


        print(f"Stored keys:\nPrivate key file:\t{private_key_path}\nPublic key file:\t{public_key_path}")


    parser.set_defaults(func=func)


def run():
    """
    Commandline utils for admins of decision API servers.

    Warning: This tool can possibly ruin your instance and has not been properly tested yet. Use with caution.
    """

    parser = argparse.ArgumentParser(description=__doc__)

    subparsers = parser.add_subparsers()

    add_authority(subparsers)

    rm_authority(subparsers)

    clean_db(subparsers)

    gen_key(subparsers)

    args = parser.parse_args()

    args.func(args)

if __name__ == "__main__":

    run()
