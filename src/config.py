from datetime import timedelta
import os
import base64
import json
import yaml
import datetime
import random
import uuid
from Crypto.Hash import SHA3_256
from Crypto.Random import get_random_bytes

SALT = os.environ.get("ACCESS_TOKEN_SALT", b"WM123aoeiaoeinsdaoensaodoeR")

ACCESS_TOKEN_EXPIRATION = timedelta(days=os.environ.get("ACCESS_TOKEN_EXPIRATION", 90))

DECISION_AUTHORITY_ID = os.environ["DECISION_AUTHORITY_ID"]

DECISION_AUTHORTIY_PRIVATE_KEY_FILE = os.environ["DECISION_AUTHORTIY_PRIVATE_KEY_FILE"]

PROHIBITED_BUILTINS_IN_VOTING_METHOD = { "breakpoint", "compile", "eval", "exec", "help", "input", "open", "print", "__import__" }

MODULES_IN_VOTING_METHOD_CODE = {

    "base64" : base64,

    "random" : random,

    "json" : json,

    "yaml" : yaml,

    "datetime" : datetime,

    "uuid" : uuid

}

JWT_PRIVATE_KEY_FILE = os.environ.get("JWT_PRIVATE_KEY_FILE", "privkey-jwt.pem")

JWT_PUBLIC_KEY = open(os.environ.get("JWT_PUBLIC_KEY_FILE", "publickey-jwt.pem")).read()

DOMAIN = os.environ.get("DOMAIN", "localhost")

OLD_AGE_DEFAULT = timedelta(days=int(os.environ.get("OLD_AGE_DEFAULT", 90)))

JOBS_INTERVAL = timedelta(seconds=10)
