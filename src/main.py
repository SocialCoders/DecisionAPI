from flask import Flask, request, jsonify
from schema import schema
from ariadne import graphql_sync
from ariadne.constants import PLAYGROUND_HTML
import os
import atexit
import db
import scheduler

app = Flask(__name__)

app.debug = "DEBUG" in os.environ

db.init()

scheduler.load()

atexit.register(scheduler.save)

scheduler.start()

@app.before_request
def before_request():

    db.database.connect(reuse_if_open=True)


@app.after_request
def after_request(response):

    db.database.close()

    return response

@app.route("/api/graphql", methods=["GET"])
def graphql_playground():

    if app.debug:

        return PLAYGROUND_HTML, 200

    else:

        return "404", 404

@app.route("/api/graphql", methods=["POST"])
def graphql_server():

    data = request.get_json()

    success, result = graphql_sync(

        schema,

        data,

        context_value=request,

        debug=app.debug

    )

    status_code = 200 if success else 400

    return jsonify(result), status_code
