import uuid

def gen_uuid(model):

    while True:

        id = uuid.uuid4()

        if model.get_or_none(model.id == id) is None:

            return id
