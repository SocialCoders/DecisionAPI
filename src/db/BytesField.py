import peewee as pw


class BytesField(pw.Field):

    field_type = "bytes"

    def db_value(self, value):

        return value

    def python_value(self, value):

        return bytes(value) if value is not None else value
