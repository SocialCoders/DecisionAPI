from .BaseModel import BaseModel
from .BytesField import BytesField
import pickle
import peewee as pw

class Job(BaseModel):
    """
    Job for execution by the scheduler
    """

    id = pw.BigAutoField(primary_key=True)

    payload = BytesField()

    time = pw.BigIntegerField()

    done = pw.BooleanField()

    priority = pw.IntegerField()

    @classmethod
    def new(cls, payload, time, priority=0):

        return cls.create(

            payload=payload.bytes,

            done=False,

            time=time.timestamp(),

            priority=priority

            )
