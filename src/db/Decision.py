from .BaseModel import BaseModel
from .Schedule import Schedule
from .Authority import Authority
from .Account import Account
from .VotingMethod import VotingMethod
from .Franchise import Franchise
from .gen_uuid import gen_uuid
from  config import DECISION_AUTHORITY_ID, DECISION_AUTHORTIY_PRIVATE_KEY_FILE
import peewee as pw
from typing import Tuple
from datetime import datetime
from Crypto.Hash import SHA3_256
from .BytesField import BytesField
import pickle
import ast


class Proposal(BaseModel):

    id = pw.UUIDField(primary_key=True)

    decision = pw.DeferredForeignKey("Decision", backref="proposals")

    proposer = pw.ForeignKeyField(Account, backref="proposals", null=True)

    payload = BytesField()

    def to_dict(self):

        return {

            "id" : self.id,

            "decision" : self.decision.id,

            "proposer" : self.proposer.id if self.proposer is not None else None,

            "payload" : self.payload

        }


class Ballot(BaseModel):

    id = pw.UUIDField(primary_key=True)

    decision = pw.DeferredForeignKey("Decision", backref="ballots")

    revoke_key = BytesField()

    payload = BytesField()

    def to_dict(self):

        return {

            "decision" : self.decision.id,

            "payload" : self.payload


        }


class Participant(BaseModel):

    id = pw.UUIDField(primary_key=True)

    decision = pw.DeferredForeignKey("Decision", backref="participants")

    account = pw.ForeignKeyField(Account, backref="participations")

    def to_dict(self):

        return {

            "id" : self.id,

            "decision" : self.decision.id,

            "account" : self.account.id

        }


class Decision(BaseModel):

    id = pw.UUIDField(primary_key=True, default = lambda: gen_uuid(Decision))

    initiator = pw.ForeignKeyField(Account, backref="initiated_decisions")

    schedule = pw.ForeignKeyField(Schedule, backref="decision")

    voting_method = pw.ForeignKeyField(VotingMethod, backref="decisions")

    voters_franchise = pw.ForeignKeyField(Franchise, backref="decision")

    proposer_franchise = pw.ForeignKeyField(Franchise, backref="decision")

    result = pw.DeferredForeignKey("DecisionResult", backref="decision", null=True)

    def status(self):

        return self.schedule.current_status()

    def propose(self, payload, proposer):

        return Proposal.create(

            id = gen_uuid(Proposal),

            decision = self,

            proposer = proposer,

            payload = payload

            )

    def __create_ballot(self, account, payload, revoke_key) -> Tuple[Participant, Ballot]:

        ballot = Ballot.create(

                id = gen_uuid(Ballot),

                decision = self,

                revoke_key = revoke_key,

                payload = payload

            )

        if account is None:

            return None, ballot

        participant = Participant.create(

                id = gen_uuid(Participant),

                decision = self,

                account = account

            )

        return participant, ballot


    def vote(self, account, payload, revoke_key) -> bool:

        if Participant.select().where(Participant.account == account).where(Participant.decision == self).exists():

            return None


        participant, ballot = self.__create_ballot(account, payload, revoke_key)

        return ballot is not None


    def overwrite_vote(self, payload, previous_revoke_key, new_revoke_key) -> bool:

        """
        When you vote, you generate some revoke key and send the hash of that revoke key as part of the ballot.

        To overwrite the ballot you submited before, you need to transfer the revoke key (unhashed this time) together with a new ballot.

        The server then deletes the old ballot (identified by the revoke_key in the request) and adds the new ballot instead.

        When you overwrite a ballot, you don't need to authenticate yourself, meaning you can do it nearly totally anonymously.

        """

        previous_revoke_key_hash = SHA3_256.new(previous_revoke_key).digest()

        ballot = Ballot.select().join(Decision).where(Ballot.decision == self).where(Ballot.revoke_key == previous_revoke_key_hash).get()

        ballot.delete_instance()

        return self.__create_ballot(None, payload, new_revoke_key)[1] is not None

    def encode(self, result):

        """Encode a decision as binary data
        """

        message = {

            "result": result,

            "ballots" : [ b.to_dict() for b in self.ballots],

            "participants" : [ p.to_dict() for p in self.participants],

            "proposals" : [ p.to_dict() for p in self.proposals],

            "voting_method" : self.voting_method.id,

            "id" : self.id

        }

        return pickle.dumps(message)

    def calculate(self):

        """
        Calculate the result of this decision, if it has concluded.
        """

        # TODO: Raise specific Exceptions for each of these cases.

        if self.status() != "CONCLUDED": # has concluded

            return False

        if self.result is not None: # has not been calculated before

            return False

        if not self.voting_method.verify(): # voting method has been certified

            return False

        if not self.voting_method.authority.is_code_auditor: # voting method has been certified by a trusted authority

            return False

        proposals = [ proposal.to_dict() for proposal in self.proposals ]

        ballots = [ ballot.to_dict() for ballot in self.ballots ]

        participants = [ participant.to_dict() for participant in self.participants ]

        proposer_franchise = [ member.id for member in self.proposer_franchise.members ]

        voter_franchise = [ member.id for member in self.voters_franchise.members ]

        result = self.voting_method.execute(proposals, ballots, participants, proposer_franchise, voter_franchise)

        calculated_at = datetime.now()

        decision_authority = Authority.get_by_id(DECISION_AUTHORITY_ID)

        signature = decision_authority.sign(self.encode(result), open(DECISION_AUTHORTIY_PRIVATE_KEY_FILE).read())

        self.result = DecisionResult.create(

                id = gen_uuid(DecisionResult),

                calculated_at = calculated_at,

                result = result,

                signature = signature,

                decision_authority = decision_authority

                )

        self.save()

        return self.result

class DecisionResult(BaseModel):

    id = pw.UUIDField(primary_key=True)

    calculated_at = pw.DateTimeField(default=datetime.now)

    result = BytesField()

    signature = BytesField()

    decision_authority = pw.ForeignKeyField(Authority, backref="results")




