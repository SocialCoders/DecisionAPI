from .BaseModel import BaseModel, database
from .Authority import Authority
from .VotingMethod import VotingMethod
from .Account import Account
from .Franchise import Franchise
from .Schedule import Schedule
from .Decision import Decision, DecisionResult, Proposal, Ballot, Participant
from .Job import Job

def init():

    database.connect()

    database.create_tables(
        [ Decision
        , DecisionResult
        , Proposal
        , Ballot
        , Participant
        , Franchise
        , Account
        , VotingMethod
        , Authority
        , Schedule
        , Franchise.members.get_through_model()
        , Job
        ])

    database.close()

