from .BaseModel import BaseModel
from .Account import Account
from .Authority import Authority
from .gen_uuid import gen_uuid
from config import PROHIBITED_BUILTINS_IN_VOTING_METHOD, MODULES_IN_VOTING_METHOD_CODE
from .BytesField import BytesField
import peewee as pw
import ast
import builtins

class CleanAST(ast.NodeTransformer):

    def visit_Import(self, node):

        return None

    def visit_ImportFrom(self, node):

        return None


class VotingMethod(BaseModel):

    id = pw.UUIDField(primary_key=True)

    name = pw.CharField()

    description = pw.TextField()

    code = pw.TextField() # critical, this code is executed and using for verification

    author = pw.ForeignKeyField(Account, backref="voting_methods")

    signature = BytesField(null=True)

    authority = pw.ForeignKeyField(Authority, backref="voting_methods", null=True)


    def verify(self) -> bool:

        return self.signature is not None and self.authority is not None and self.authority.is_code_auditor and self.authority.verify(self.code.encode("utf-8"), self.signature)


    def sign(self, privateKey) -> bytes:

        return self.authority.sign(self.code.encode("utf-8"), privateKey)


    @classmethod
    def add_unverified(cls, name, description, code, author):

        return cls.create(

            id=gen_uuid(cls),

            name=name,

            description=description,

            code=code,

            author=author

        )

    def clean_code(self):

        tree = ast.parse(self.code)

        return ast.unparse(CleanAST().visit(tree))


    def execute(self, proposals, ballots, participants, proposer_franchise, voter_franchise) -> bytes:

        """
        Execute this voting method in a secure manner.

        Restrictions on the code:
        - It must be veified by a code auditor
        - It must not contain any import statement
        - builtins are stripped of any I/O operations. (see PROHIBITED_BUILTINS_IN_VOTING_METHOD)
        - only the LIBRARIES_FOR_VOTING_METHOD_CODE are available in the source code.
        - It must contain a function definition for the `count` function.

        Use this template to define your own voting method.

        ```python

        def count(proposals : frozenset, ballots : frozenset, participants : frozenset, proposer_franchise : frozenset, voter_franchise : frozenset ) -> bytes:

            return b"Result"

        ```
        """

        if not self.verify():

            return None

        safe_code = self.clean_code()

        builtins_dict = vars(builtins)

        safe_globals = {

            "__builtins__": {
                    name: builtins_dict[name]

                    for name in builtins_dict

                    if name not in PROHIBITED_BUILTINS_IN_VOTING_METHOD
            }
        } | MODULES_IN_VOTING_METHOD_CODE

        safe_locals = dict()

        exec(safe_code, safe_globals, safe_locals)

        count_fn = safe_locals.get("count", lambda proposals, ballots, participants, propoposer_franchise, voter_franchise: b"")

        result = bytes(count_fn(proposals, ballots, participants, proposer_franchise, voter_franchise))

        return result



