# SPDX-FileCopyrightText: 2022 CSDUMMI <csdummi.misquality@simplelogin.co>
#
# SPDX-License-Identifier: AGPL-3.0-or-later
import peewee as pw
import os

DATABASE = os.environ["DB_NAME"]

if os.environ["DB_TYPE"] == "postgres":

    PASSWORD = os.environ["DB_PASSWORD"]

    USER = os.environ["DB_USER"]

    HOST = os.environ["DB_HOST"]

    database = pw.PostgresqlDatabase(DATABASE,password=PASSWORD, user=USER, host=HOST, field_types={"bytes": "bytea"})

else:

    database = pw.SqliteDatabase(DATABASE, field_types={"bytes": "blob"})


class BaseModel(pw.Model):

    class Meta:

        database = database
