from .BaseModel import BaseModel
from Crypto.PublicKey import RSA
from Crypto.Signature import pss
from Crypto.Hash import SHA3_256
from .gen_uuid import gen_uuid
from .BytesField import BytesField
import peewee as pw

class Authority(BaseModel):

    """
    Authorities are authenticated using their public key pair.

    Using this key pair to sign either a decision result or a voting method's code.

    By signing a decision result, the authority assures that is indeed the result of the decision when using the specified voting method.

    By signing the code of a voting method, the authority assures that the code itself is safe to execute and does not take up inadequate amounts of resources.
    """

    id = pw.UUIDField(primary_key=True, default=lambda: gen_uuid(Authority))

    name = pw.CharField()

    public_key = BytesField()

    abilities = pw.BitField()

    is_decision_authority = abilities.flag(0)

    is_code_auditor = abilities.flag(1)


    def verify(self, message : bytes, signature : bytes) -> bool:

        public_key = RSA.import_key(self.public_key)

        verifier = pss.new(public_key)

        try:

            verifier.verify(SHA3_256.new(message), signature)

            return True

        except ValueError:

            return False


    def sign(self, message : bytes, private_key) -> bytes:

        if type(private_key) is not RSA.RsaKey:

            private_key = RSA.import_key(private_key)

        if private_key.publickey() != RSA.import_key(self.public_key):

            raise ValueError(private_key)

        signer = pss.new(private_key)

        return signer.sign(SHA3_256.new(message))
