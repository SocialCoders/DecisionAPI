from .BaseModel import BaseModel
from config import ACCESS_TOKEN_EXPIRATION, SALT, JWT_PRIVATE_KEY_FILE, JWT_PUBLIC_KEY,DOMAIN
from exceptions import *
from Crypto.Hash import SHA3_256
from Crypto.Random import get_random_bytes
from datetime import datetime
import base64
import peewee as pw
import jwt
from functools import wraps
from flask import request
from .gen_uuid import gen_uuid
from enum import Enum
from typing import Any

def list_get_or_none(obj):

    if obj == []:

        return None

    return obj.pop()

class TokenType(Enum):

    """
    An access token can be used for any action, except to generate an access token. It expires ACCESS_TOKEN_EXPIRATION after generation.

    An authorization token can only be used to generate access tokens. It never expires.
    """

    ACCESS = "access"

    AUTHORIZATION = "authorization"


class Account(BaseModel):
    """
    Accounts are used to authenticate using access tokens, generated with an authorization token (generated only once).
    """

    id = pw.UUIDField(primary_key=True)

    name = pw.CharField(null=True)

    flags = pw.BitField()

    authorization_token_issued = flags.flag(1)

    @classmethod
    def require(cls, token_type, g):

        def outer(endpoint):

            @wraps(endpoint)
            def inner(*args, **kwargs):

                authorization = request.headers.get("Authorization", "").split()

                if len(authorization) != 2:

                    return None

                authorization = authorization[1]

                if result := cls.is_valid_token(authorization):

                    if result is None:

                        return None

                    _, acccount, decoded = result

                    g.account = result[1]

                    if TokenType(decoded["type"]) != TokenType(token_type):

                        return None

                    return endpoint(*args, **kwargs)

                return None

            return inner

        return outer


    def generate_access_token(self):

        return self.generate_token(TokenType.ACCESS)

    def generate_authorization_token(self):

        if self.authorization_token_issued:

            raise AuthorizationTokenIssuedError

        self.authorization_token_issued = True

        self.save()

        return self.generate_token(TokenType.AUTHORIZATION)

    def generate_token(self, token_type):

        encoded = jwt.encode({

            "iss": DOMAIN,

            "sub" : str(self.id),

            "type" : token_type.value,

            "exp": (datetime.now() + ACCESS_TOKEN_EXPIRATION).timestamp()

        }, open(JWT_PRIVATE_KEY_FILE).read(), algorithm="RS256")

        return encoded


    @classmethod
    def is_valid_token(cls, encoded):

        """
        Checks if the JWT Token is (still) valid.

        Returns None if the token is invalid, otherwise returns a triplet of TokenType, Account and decoded token.
        """

        decoded = jwt.decode(encoded, JWT_PUBLIC_KEY, algorithms=["RS256"], require=["iss", "sub", "exp", "type"], issuer=DOMAIN)

        decoded["type"] = TokenType(decoded["type"])

        account = cls.get_or_none(cls.id == decoded["sub"])

        if account is None:

            return None

        return decoded["type"], account, decoded


    def create_one(name):

        return Account.create(

            id = gen_uuid(Account),

            name = name,

            authorization_token_issued = False

        )

    def create_many(size, names = []):

        return [

            Account.create_one(list_get_or_none(names))

            for i in range(size)

            ]


def hash_token(data : bytes) -> bytes:

    return SHA3_256.new(data + SALT).digest()
