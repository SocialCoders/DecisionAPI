from .BaseModel import BaseModel
from .Account import Account
from .gen_uuid import gen_uuid
import peewee as pw

class Franchise(BaseModel):

    id = pw.UUIDField(primary_key=True, default=lambda: gen_uuid(Franchise))

    members = pw.ManyToManyField(Account, backref="franchises")


