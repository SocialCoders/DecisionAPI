from .BaseModel import BaseModel
from datetime import datetime
import peewee as pw


class Schedule(BaseModel):

    start_proposal_phase = pw.DateTimeField()

    end_proposal_phase = pw.DateTimeField()

    start_voting_phase = pw.DateTimeField()

    end_voting_phase = pw.DateTimeField()

    def current_status(self):

        now = datetime.now()

        if now < self.start_proposal_phase:

            return "NOTSTARTED"

        if now > self.start_proposal_phase and now < self.end_proposal_phase:

            return "PROPOSALPHASE"

        if now > self.end_proposal_phase and now < self.start_voting_phase:

            return "WAITING"

        if now > self.start_voting_phase and now < self.end_voting_phase:

            return "VOTINGPHASE"

        return "CONCLUDED"
