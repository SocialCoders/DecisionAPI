from ariadne import MutationType
from flask import request, g
from .auth import authenticated
import db
import scheduler

mutation = MutationType()

@mutation.field("create")
@db.Account.require("access", g)
def resolve_create(_, info, create_decision):

    initiator = g.account

    voting_method = db.VotingMethod.get_or_none(db.VotingMethod.id == create_decision["voting_method"])

    if voting_method is None:

        return None

    schedule = db.Schedule.create(

        start_proposal_phase = create_decision["schedule"]["start_proposal_phase"],

        end_proposal_phase = create_decision["schedule"]["end_proposal_phase"],

        start_voting_phase = create_decision["schedule"]["start_voting_phase"],

        end_voting_phase = create_decision["schedule"]["end_voting_phase"]

        )

    voters_franchise = db.Franchise.create()

    voters_franchise.members.add(db.Account.select().where(db.Account.id << create_decision["voters_franchise"]))

    proposer_franchise = db.Franchise.create()

    proposer_franchise.members.add(db.Account.select().where(db.Account.id << create_decision["proposer_franchise"]))

    decision = db.Decision.create(

        initiator = initiator,

        voting_method = voting_method,

        schedule = schedule,

        voters_franchise = voters_franchise,

        proposer_franchise = proposer_franchise,

        result = None

        )

    def calc(job_id=None):

        print("Calculating", decision.id)

        decision.calculate()

    scheduler.add(schedule.end_voting_phase.timestamp(), calc)

    return decision


@mutation.field("remove")
@db.Account.require("access", g)
def resolve_remove(_, info, decision):

    decision = db.Decision.get_or_none(db.Decision.id == decision)

    if decision is None:

        return None

    initiator = g.account

    if decision.status() == "NOTSTARTED" and initiator == decision.initiator:

        decision.delete_instance()

        return True

    return False


@mutation.field("propose")
@db.Account.require("access", g)
def resolve_propose(_, info, decision, proposal):

    decision = db.Decision.get_or_none(db.Decision.id == decision)

    if decision is None:

        return None

    account = g.account

    if decision.status() != "PROPOSALPHASE" or account not in decision.proposer_franchise.members:

        return None

    return decision.propose(proposal["payload"], account)


@mutation.field("rescind_proposal")
@db.Account.require("access", g)
def resolve_rescind_proposal(_, info, decision, proposal):

    account = g.account

    decision = db.Decision.get_by_id(decision)

    proposal = db.Proposal.get_by_id(proposal)

    if proposal.proposer != account or proposal.decision != decision or decision.status() != "PROPOSALPHASE":

        return False

    proposal.delete_instance()

    return True

# vote

@mutation.field("vote")
@db.Account.require("access", g)
def resolve_vote(_, info, decision, ballot):

    account = g.account

    decision = db.Decision.get_by_id(decision)

    return decision.vote(account, ballot["payload"], ballot["revoke_key"])

@mutation.field("overwrite_vote")
def resolve_overwrite_vote(_, info, decision, ballot, overwrite_key):

    decision = db.Decision.get_by_id(decision)

    return decision.overwrite_vote(ballot["payload"], overwrite_key, ballot["revoke_key"])


# voting method

@mutation.field("add_voting_method")
@db.Account.require("access", g)
def resolve_add_voting_method(_, info, voting_method):

    account = g.account

    return db.VotingMethod.add_unverified(

        name = voting_method["name"],

        description = voting_method["description"],

        code = voting_method["code"],

        author = account

        )

@mutation.field("certify_voting_method")
def resolve_certify_voting_method(_, info, voting_method, authority, signature):

    voting_method = db.VotingMethod.get_by_id(voting_method)

    if voting_method.verify():

        return False

    authority = db.Authority.get_by_id(authority)

    if not authority.is_code_auditor:

        return False

    voting_method.authority = authority

    voting_method.signature = signature

    voting_method.save()

    return voting_method.verify()

# accounts

@mutation.field("create_account")
def resolve_create_account(_, info, name = None):

    return db.Account.create_one(name)


@mutation.field("create_many_accounts")
def resolve_create_many(_, info, size, names = []):

    return db.Account.create_many(size, names)


@mutation.field("create_authorization_code")
def resolve_create_authorization_code(_, info, id):

    account = db.Account.get_by_id(id)

    return account.generate_authorization_token()


@mutation.field("create_access_token")
@db.Account.require("authorization", g)
def resolve_create_access_token(_, info):

    print("create_access_token called")

    account = g.account

    access_token = account.generate_access_token()

    print("Access Token for", access_token)

    return access_token


@mutation.field("set_account_name")
@db.Account.require("access", g)
def resolve_set_account_name(_, info, name):

    account = g.account

    account.name = name

    account.save()

    return account


