from ariadne import ObjectType

voting_method = ObjectType("VotingMethod")

@voting_method.field("verified")
def resolve_verified(obj, info):

    return obj.verify()
