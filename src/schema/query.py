from ariadne import QueryType
import db

query = QueryType()

@query.field("decision")
def resolve_decision(_, info, id):

    return db.Decision.get_by_id(id)

@query.field("decisions")
def resolve_decisions(_, info, status = None):

    decisions = db.Decision.select()

    if status is not None:

        decisions = decisions.where(db.Decision.status == status)

    return decisions

@query.field("account")
def resolve_account(_, info, id):

    return db.Account.get_or_none(db.Account.id == id)


@query.field("voting_method")
def resolve_voting_method(_, info, id):

    return db.VotingMethod.get_or_none(db.VotingMethod.id == id)


@query.field("voting_methods")
def resolve_voting_methods(_, info, verified = True):

    return [v for v in db.VotingMethod.select() if v.verify() == verified]

