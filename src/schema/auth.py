from functools import wraps
from flask import request
import db

def authenticated(f):

    @wraps(f)
    def decorated(*args, **kwargs):

        account = db.Account.get_from_request(request)

        if account is None:

            return None

        else:

            return f(*args, **kwargs)

    return decorated

