from ariadne import ScalarType
from Crypto.PublicKey import ECC
from datetime import datetime
import base64

datetime_scalar = ScalarType("DateTime")

@datetime_scalar.serializer
def serialize_datetime(value):

    return int(value.timestamp())

@datetime_scalar.value_parser
def parse_datetime(value):

    return datetime.fromtimestamp(int(value))

def serialize_bytes(value : bytes) -> str:

    return base64.b64encode(value, b"-_").decode("utf-8")


def parse_bytes(value : str) -> bytes:

    return base64.b64decode(value.encode("utf-8"), b"-_")


bytes_scalar = ScalarType("Bytes",  serializer=serialize_bytes, value_parser=parse_bytes)

hash_scalar = ScalarType("Hash", serializer=serialize_bytes, value_parser=parse_bytes)

signature_scalar = ScalarType("Signature", serializer=serialize_bytes, value_parser=parse_bytes)


public_key_scalar = ScalarType("PublicKey")

@public_key_scalar.serializer
def serialize_public_key(value):

    return value.export_key("PEM").decode("utf-8")

@public_key_scalar.value_parser
def parse_public_key(value):

    return ECC.import_key(value)
