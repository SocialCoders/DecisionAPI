from ariadne import make_executable_schema, load_schema_from_path
from .query import query
from .mutation import mutation
from .decision import decision
from .voting_method import voting_method
from .scalars import datetime_scalar, bytes_scalar, hash_scalar, signature_scalar, public_key_scalar

schema = make_executable_schema(load_schema_from_path("../graphql"), query, mutation, decision, voting_method, datetime_scalar, bytes_scalar, hash_scalar, signature_scalar)
