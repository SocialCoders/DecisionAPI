from ariadne import ObjectType

decision = ObjectType("Decision")

@decision.field("ballots")
def resolve_ballots(decision, info):

    if decision.status() == "CONCLUDED":

        return decision.ballots

    else:

        return None

@decision.field("status")
def resolve_status(decision, info):

    return decision.status()
