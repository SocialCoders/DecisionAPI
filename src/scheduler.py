import db
import time
from datetime import datetime, timedelta
from config import JOBS_INTERVAL
import threading
import pickle
import sched
import time
import atexit
import uuid
import logging

logging.basicConfig(level=logging.INFO)

def calc_decision_result(job=None, job_id=None):

    id = uuid.UUID(bytes=job.payload)

    logging.info(f"Calculate decision {id}")

    decision = db.Decision.get_by_id(id)

    decision.calculate()

    job.done = True

    job.save()


class Scheduler:

    def __init__(self):

        """
        Homebrew scheduler storing jobs in the db
        and loading them from there.
        """

        self.__scheduler = sched.scheduler(time.time, time.sleep)

        self.__loaded = 0

        self.load()

        logging.info("%s jobs loaded", self.__loaded)


    def load(self):

        for job in db.Job.select().where(db.Job.id >= self.__loaded).where(~db.Job.done):

            self.__scheduler.enterabs(job.time, job.priority, calc_decision_result, kwargs={"job_id": job.id, "job": job })

            self.__loaded = max(job.id, self.__loaded)


    def clean_jobs(self):

        active_jobs = self.__scheduler.queue

        active_jobs_ids = list(map(lambda job: job.kwargs["job_id"], active_jobs))

        db.Job.delete().where(~(db.Job.id << active_jobs_ids) | db.Job.done).execute()


    def save(self):

        self.clean_jobs()

        active_jobs = self.__scheduler.queue

        for job in active_jobs:

            if db.Job.get_or_none(db.Job.id == job.kwargs["job_id"]) is None:

                db.Job.create(

                    id=job.kwargs["job_id"],

                    payload=job.kwargs["payload"],

                    time=job.time,

                    priority=job.priority)


    def run(self):

        while True:

            self.__scheduler.run()

            self.load()

            time.sleep(JOBS_INTERVAL.total_seconds())



if "__main__" == __name__:

    logging.info("Initializing scheduler...")

    db.database.connect()

    scheduler = Scheduler()


    logging.info("Starting scheduler...")

    try:

        scheduler.run()

    finally:

        scheduler.save()

    logging.info("Stopped scheduler")



