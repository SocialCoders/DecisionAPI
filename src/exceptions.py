class AuthorizationTokenIssuedError(Exception):

    pass


class InvalidAccessError(Exception):

    pass


class InvalidAuthorizationError(Exception):

    pass
