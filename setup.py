#!/usr/bin/env python

from distutils.core import setup

setup(name="DecisionAPI",
      version="1.0",
      description="DecisionAPI",
      author="CSDUMMI",
      author_email="csdummi.misquality@simplelogin.co",
      url="https://codeberg.org/SocialCoders/DecisionAPI"

      )
