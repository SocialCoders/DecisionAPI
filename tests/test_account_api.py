from gql import gql
from .utils import queries
from . import utils

def test_create_account():


    name = utils.random_string(20)

    id, authorization_token, access_token = utils.account.create(name)

    account = utils.client.execute(queries.get_account, variable_values={ "account_id" : id })["account"]

    assert account["id"] == id

    assert account["name"] == name

    utils.account.authenticate(access_token)

    new_name = utils.random_string(20)

    new_account = utils.client.execute(queries.set_account_name, variable_values={"name" : new_name })["set_account_name"]

    assert account["id"] == new_account["id"]

    assert new_account["name"] == new_name


