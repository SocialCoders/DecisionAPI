from gql import gql
from . import utils
from .utils import queries
from Crypto.PublicKey import RSA
from Crypto.Signature import pss
from Crypto.Hash import SHA3_256
from datetime import datetime, timedelta
import pickle
import time
import random
import os

AUDITOR_AUTHORITY_PRIVATE_KEY = RSA.import_key(open(os.environ.get("AUDITOR_AUTHORITY_KEY")).read())

AUDITOR_AUTHORITY_ID = os.environ["AUDITOR_AUTHORITY_ID"]

def test_add_voting_method():

    account_id, authorization_token, access_token = utils.account.create(utils.random_string(5))

    utils.client.transport.headers["Authorization"] = f"Bearer {access_token}"

    name = utils.random_string(20)

    description = utils.random_string(500)

    code = """

def count(proposals, ballots, participants, proposer_franchise, voter_franchise) -> bytes:

    try:

        return proposals.pop()["payload"]

    except KeyError:

        return b"Error"

    """

    voting_method = utils.client.execute(queries.add_voting_method, variable_values = {

            "name" : name,

            "description" : description,

            "code" : code

        })["add_voting_method"]


    assert voting_method["id"] is not None

    assert voting_method["name"] == name

    assert voting_method["description"] == description

    assert voting_method["code"] == code

    assert voting_method["author"]["id"] == account_id

    assert voting_method["authority"] is None

    #assert voting_method["signature"] is None

    signature = sign(code.encode("utf-8"), AUDITOR_AUTHORITY_PRIVATE_KEY)

    is_certified = utils.client.execute(queries.certify_voting_method, variable_values={

        "voting_method" : voting_method["id"],

        "authority_id": AUDITOR_AUTHORITY_ID,

        "signature" : signature

    })["certify_voting_method"]

    assert is_certified

    certified_voting_method = utils.client.execute(queries.get_voting_method, variable_values={

        "voting_method" : voting_method["id"]

        })["voting_method"]

    assert certified_voting_method["id"] == voting_method["id"]

    assert certified_voting_method["verified"]

    assert certified_voting_method["authority"]["id"] == AUDITOR_AUTHORITY_ID



def test_create_decision():

    phase = timedelta(seconds=2)

    decision, _, _ = utils.decision.create(random.randint(2,100), phase=phase)

    # STATUS changes through time
    STATUSES = ["NOTSTARTED", "PROPOSALPHASE", "WAITING", "VOTINGPHASE", "CONCLUDED"]

    for STATUS in STATUSES:

        status = utils.client.execute(gql("query GetStatus($id: ID!) { decision(id: $id) { status } }"), variable_values={ "id" : decision["id"] })["decision"]["status"]

        assert status == STATUS

        time.sleep(phase.total_seconds())

    assert status == "CONCLUDED"

    # decision test after initialization

    assert decision["id"] is not None

    assert decision["ballots"] is None

    assert decision["proposals"] == []

    assert decision["participants"] == []

    # concluded decision -> published ballots

    decision = utils.client.execute(queries.get_decision, variable_values={ "decision" : decision["id"] })["decision"]

    assert decision["ballots"] == []

    assert decision["status"] == "CONCLUDED"


def test_propose_and_vote():

    phase = timedelta(seconds=2)

    decision, proposer_franchise, voters_franchise = utils.decision.create(5, phase=phase)

    initiator = proposer_franchise[0]

    authorization_token = utils.account.get_authorization_token(initiator)

    access_token = utils.account.get_access_token(authorization_token)

    utils.account.authenticate(access_token)

    utils.decision.wait_for_status("PROPOSALPHASE", decision["id"], before=["NOTSTARTED"], after=["WAITING", "VOTINGPHASE", "CONCLUDED"], phase=phase)

    proposal_payload = random.randbytes(32)

    proposal = utils.client.execute(queries.create_proposal, variable_values={ "decision" : decision["id"], "payload" : proposal_payload })["propose"]

    assert proposal is not None

    utils.decision.wait_for_status("VOTINGPHASE", decision["id"], before=["PROPOSALPHASE", "WAITING"], after=["CONCLUDED", "NOTSTARTED"], phase=phase)

    ballot_payload = random.randbytes(32)

    ballot_revoke_key = random.randbytes(32)

    result = utils.client.execute(queries.vote, variable_values={"decision" : decision["id"], "payload" : ballot_payload, "revoke_key" : SHA3_256.new(ballot_revoke_key).digest() })

    print(result)

    assert result["vote"]

    overwrite_ballot_payload = random.randbytes(32)

    new_revoke_key = random.randbytes(32)

    assert utils.client.execute(queries.overwrite_vote, variable_values={"decision" : decision["id"], "payload" : overwrite_ballot_payload, "revoke_key": SHA3_256.new(new_revoke_key).digest(), "overwrite_key" : ballot_revoke_key})["overwrite_vote"]

    utils.decision.wait_for_status("CONCLUDED", decision["id"], before=["NOTSTARTED", "PROPOSALPHASE", "WAITING", "VOTINGPHASE"], phase=phase)

    decision = utils.client.execute(queries.get_decision, variable_values={"decision" : decision["id"] })["decision"]

    assert decision["status"] == "CONCLUDED"

    assert len(decision["ballots"]) == 1

    assert decision["ballots"][0]["payload"] == overwrite_ballot_payload

    assert len(decision["participants"]) == 1

    assert decision["participants"][0]["account"]["id"] == initiator

    assert len(decision["proposals"]) == 1

    assert decision["proposals"][0]["payload"] == proposal_payload

    assert decision["proposals"][0]["id"] == proposal["id"]

    assert decision["result"]["result"] == proposal_payload

    message = decision["result"]["result"] + pickle.dumps(calculated_at.timestamp())

    assert verify(message, decision["result"]["signature"], decision["result"]["public_key"])

def verify(message, signature, public_key):

    public_key = RSA.import_key(public_key)

    verifier = pss.new(public_key)

    try:

        verifier.verify(SHA3_256.new(message), signature)

        return True

    except ValueError:

        return False

def sign(message, private_key):

    if private_key is bytes:

        private_key = RSA.import_key(private_key)

    return pss.new(private_key).sign(SHA3_256.new(message))


def test_query_all_decisions():

    query = gql(
        """

        {
            decisions {
                id
            }
        }

        """)

    result = utils.client.execute(query)

    for decision in result["decisions"]:

        assert "id" in decision

        query = gql(
            """
            query getDecision($id: ID!) {
                decision(id: $id) {
                    id
                }
            }
            """)

        fetched_decision = utils.client.execute(query, variable_values = { "id" : decision["id"] })["decision"]

        assert decision["id"] == fetched_decision["id"]

