from gql import gql

get_decision = gql("""

query GetDecision($decision: ID!) {

  decision(id: $decision) {

    status

    ballots { payload }

    proposals {

      payload

      id

    }

    participants { account { id } }

    result {

      signature

      decision_authority {

        public_key

      }

      result

    }

  }
}

""")

get_status = gql("""

query GetStatus($decision : ID!) {

  decision(id: $decision) {

    status

  }

}

""")

create_decision = gql("""

mutation createDecision($voting_method :ID!
                        , $start_proposal_phase:DateTime
                        , $end_proposal_phase :DateTime
                        , $start_voting_phase :DateTime
                        , $end_voting_phase :DateTime
                        , $voters_franchise :[ID]!
                        , $proposer_franchise : [ID]!) {

  create (create_decision : {
  	voting_method: $voting_method
    schedule: {
    	start_proposal_phase :$start_proposal_phase

      end_proposal_phase :$end_proposal_phase

      start_voting_phase : $start_voting_phase

      end_voting_phase :$end_voting_phase

    }

    voters_franchise : $voters_franchise

    proposer_franchise : $proposer_franchise

  }) {
  	id
    voting_method { id }
    status
    schedule {
      start_proposal_phase
      end_proposal_phase

    	start_voting_phase
      end_voting_phase
    }

    voters_franchise { members { id } }
    proposer_franchise { members { id } }

    proposals { id, proposer { id } }

    participants { account { id } }

    ballots { payload }

  }

}

""")

create_franchise = gql("""

mutation createFranchise($size:Int!) {

  create_many_accounts(size:$size)

}

""")

create_account = gql("""

mutation CreateAccount($name:String) {

  create_account(name:$name)

}

""")

add_voting_method = gql("""

mutation addVotingMethod($name:String!, $description:String!, $code:String!) {

  add_voting_method(voting_method: {

    name: $name

    description:$description

    code : $code

  }) {

  	id

    name

    description

    code

    author { id }

    authority { id }

    signature

  }
}

""")

certify_voting_method = gql("""

mutation CertifyVotingMethod($voting_method : ID!, $authority_id: ID!, $signature: Bytes!) {

  certify_voting_method(voting_method : $voting_method, authority: $authority_id signature : $signature)

}

""")

get_voting_method = gql("""

query GetVotingMethod($voting_method : ID!) {

  voting_method(id : $voting_method) {

    id

    authority { id }

    verified

  }

}

""")

create_authorization_token = gql("""

mutation getAuthorizationToken($id:ID!) {

  create_authorization_code(id:$id)

}

""")

create_access_token = gql("""

mutation getAccessToken {

  create_access_token

}

""")

set_account_name = gql("""

mutation setAccountName($name:String!) {

  set_account_name(name:$name) {

    id

    name

  }

}

""")

get_account = gql("""

query getAccount($account_id:ID!){
	account(id:$account_id) {
    id
  	name
  }
}

""")

add_voting_method = gql("""

mutation addVotingMethod($name:String!, $description:String!, $code:String!) {

  add_voting_method(voting_method: {

    name: $name

    description:$description

    code : $code

  }) {
  	id
    name
    description
    code
    author { id }
    authority { id }

  }
}

""")

create_proposal = gql("""

mutation propose($decision:ID!, $payload: Bytes!) {

  propose(decision:$decision, proposal: {payload: $payload}) {

    id

    payload

    proposer { id }

  }

}

""")

vote = gql("""

mutation vote($decision:ID!, $payload: Bytes!, $revoke_key:Hash!) {

  vote(decision:$decision, ballot: {payload: $payload, revoke_key:$revoke_key})

}

""")

overwrite_vote = gql("""

mutation overwrite_vote($decision:ID!, $payload : Bytes!, $revoke_key: Hash!, $overwrite_key: Bytes!) {

    overwrite_vote(decision : $decision, ballot : {
      payload: $payload,
      revoke_key : $revoke_key,
    }, overwrite_key : $overwrite_key)

}

""")
