from gql import gql
from .client import client
from .queries import create_decision, get_status
from datetime import timedelta, datetime
import time

def create(franchise_size, phase = timedelta(seconds=2)):

    create_franchise_mutation = gql(
        """
        mutation createFranchise($size:Int!) {

            create_many_accounts(size:$size)
        }
        """)


    proposer_franchise = client.execute(create_franchise_mutation, variable_values = { "size" : franchise_size })["create_many_accounts"]

    voters_franchise = client.execute(create_franchise_mutation, variable_values= { "size" : franchise_size })["create_many_accounts"]

    assert len(proposer_franchise) == franchise_size

    assert len(voters_franchise) == franchise_size

    voting_methods = client.execute(gql("""
        {

            voting_methods {
                id
            }

        }

        """))["voting_methods"]

    assert voting_methods != []

    voting_method = voting_methods.pop()

    start_proposal_phase = datetime.now() + phase

    end_proposal_phase = start_proposal_phase + phase

    start_voting_phase = end_proposal_phase + phase

    end_voting_phase = start_voting_phase + phase

    decision = client.execute(create_decision, variable_values={

            "voting_method" : voting_method["id"],

            "start_proposal_phase" : start_proposal_phase,

            "end_proposal_phase" : end_proposal_phase,

            "start_voting_phase" : start_voting_phase,

            "end_voting_phase" : end_voting_phase,

            "voters_franchise" : voters_franchise,

            "proposer_franchise" : proposer_franchise

        })["create"]

    assert decision["voting_method"]["id"] == voting_method["id"]

    return decision, proposer_franchise, voters_franchise

def wait_for_status(goal, id, before=[], after=[], phase=timedelta(seconds=2)):

    while True:

        status = client.execute(get_status, variable_values={ "decision" : id })["decision"]["status"]

        if status == goal:

            break

        assert status in before

        assert status not in after

        time.sleep(phase.total_seconds()/20)

