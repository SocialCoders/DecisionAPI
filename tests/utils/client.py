from gql import Client
from graphql import GraphQLScalarType
from gql.transport.requests import RequestsHTTPTransport
from gql.utilities import update_schema_scalars
from Crypto.PublicKey import DSA
from datetime import datetime
import base64
import os

API_ENDPOINT = os.environ["API_ENDPOINT"]

transport = RequestsHTTPTransport(url=API_ENDPOINT, verify=not API_ENDPOINT.startswith("https://localhost"), retries=3, headers=dict())

client = Client(transport=transport, fetch_schema_from_transport=True, parse_results=True, serialize_variables=True)

def serialize_bytes(value : bytes) -> str:

    return base64.b64encode(value, b"-_").decode("utf-8")


def parse_bytes(value : str) -> bytes:

    return base64.b64decode(value.encode("utf-8"), b"-_")


BytesScalar = GraphQLScalarType(
    name="Bytes",
    serialize=serialize_bytes,
    parse_value=parse_bytes)

HashScalar = GraphQLScalarType(
    name="Hash",
    serialize=serialize_bytes,
    parse_value=parse_bytes)

def serialize_datetime(value : datetime) -> int:

    return int(value.timestamp())


def parse_datetime(value : int) -> datetime:

    return datetime.fromtimestamp(value)

DateTimeScalar = GraphQLScalarType(
    name="DateTime",
    serialize=serialize_datetime,
    parse_value=parse_datetime)


def serialize_public_key(value) -> str:

    return value.export_key("PEM").decode("utf-8")


def parse_public_key(value):

    return DSA.import_key(value)

PublicKeyScalar = GraphQLScalarType(
    name="PublicKey",
    serialize=serialize_public_key,
    parse_value=parse_public_key)

client.connect_sync()

update_schema_scalars(client.schema, [BytesScalar, HashScalar, PublicKeyScalar, DateTimeScalar])

client.close_sync()
