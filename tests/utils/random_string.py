import string
import random

def random_string(n=20):

    return "".join(random.sample(string.ascii_letters, k = 20))
