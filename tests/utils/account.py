from .queries import create_account, create_authorization_token, create_access_token
from .client import client

def create(name = None):

    id = client.execute(create_account, variable_values={"name"  : name })["create_account"]

    authorization_token = get_authorization_token(id)

    access_token = get_access_token(authorization_token)

    client.transport.headers["Authorization"] = None

    return id, authorization_token, access_token


def get_authorization_token(id):

    return client.execute(create_authorization_token, variable_values={"id" : id })["create_authorization_code"]


def get_access_token(authorization_token):

    authenticate(authorization_token)

    return client.execute(create_access_token)["create_access_token"]


def authenticate(token):

    client.transport.headers["Authorization"] = f"Bearer {token}"
