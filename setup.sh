#!/usr/bin/env bash
./start-server.sh
./admin gen-key authority
./admin gen-key jwt # JWT support RSA, not DSA
./admin add-authority --name authority --is-auditor --is-decision-authority /keys/authority.pub
