# Decision API

An API to allow for **secure**, **certified** and **extensible** Internet Voting.

## Setup

Install `docker` and `docker-compose` and run:

```bash
$ ./setup.sh
...
Generated authority <authority id>
```

Open `.env` file and fill in:

```env
# Decision API config
DECISION_AUTHORITY_ID=<authority id>
DECISION_AUTHORTIY_PRIVATE_KEY_FILE=/keys/authority

# JWT Public Key for signing
JWT_PRIVATE_KEY_FILE=/keys/jwt
JWT_PUBLIC_KEY_FILE=/keys/jwt.pub

# change this, if you don't run on localhost
# DOMAIN=localhost

# DB config
DB_NAME=DAPI
DB_USER=decision_app
DB_PASSWORD=<random string>

# Test config

AUDITOR_AUTHORITY_ID=<authority id>
AUDITOR_AUTHORITY_KEY=keys/authority

API_ENDPOINT="https://localhost/api/graphql" # set this to the server address, you are testing.
```

And after completing this configuration, restart the server through:
```bash
$ ./start-server.sh
```

## Tests
After starting the server,
run the tests like this:

```bash
$ ./run-tests.sh
```
